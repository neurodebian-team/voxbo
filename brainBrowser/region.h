
// region.h
// Copyright (c) 2010 by The VoxBo Development Team

// This file is part of VoxBo
// 
// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
// 
// original version written by Dongbo Hu

/*******************************************************************************
 * regionRec class definition: hold each region's information
 *******************************************************************************/
using namespace std;

#include <string>
#include <string.h>
#include <time.h>

class regionRec
{
 public:
  regionRec() { clear(); }

  regionRec(void *buffer) { setData(buffer); }

  /* Use an input data pointer to set each variable. */ 
  void setData(void *buffer) {
    char *buf = (char *) buffer;
    int longLen = sizeof(long);

    ID = *((long *) buf);
    bufLen = longLen;

    name_space = buf + bufLen;
    bufLen += name_space.size() + 1;

    name = buf + bufLen;
    bufLen += name.size() + 1;

    abbrev = buf + bufLen;
    bufLen += abbrev.size() + 1;

    orgID = *((long *) (buf + bufLen));
    bufLen += longLen;
    
    source = buf + bufLen;
    bufLen += source.size() + 1;

    pFlag = buf + bufLen;
    bufLen += pFlag.size() + 1;

    link = buf + bufLen;
    bufLen += link.size() + 1;

    creator = buf + bufLen;
    bufLen += creator.size() + 1;

    addDate = *((long *) (buf + bufLen));
    bufLen += longLen;

    modifier = buf + bufLen;
    bufLen += modifier.size() + 1;

    modDate = *((long *) (buf + bufLen));
    bufLen += longLen;
  }

  /* Initialize data members. */
  void clear() {
    ID = 0;
    name_space = name = abbrev = "";
    orgID = 0;
    source = pFlag = link = "";
    creator = modifier = "";
    addDate = modDate = 0;
  }

  /* Return combined data members as a single contiguous memory location. */
  char * getBuffer() {
    memset(databuf, 0, 1000);
    bufLen = 0;
    int longLen = sizeof(long);

    memcpy(databuf, &ID, longLen);
    bufLen += longLen;

    packString(databuf, name_space);
    packString(databuf, name);
    packString(databuf, abbrev);

    memcpy(databuf + bufLen, &orgID, longLen);
    bufLen += longLen;

    packString(databuf, source);
    packString(databuf, pFlag);
    packString(databuf, link);

    packString(databuf, creator);
    memcpy(databuf + bufLen, &addDate, longLen);
    bufLen += longLen;

    packString(databuf, modifier);
    memcpy(databuf + bufLen, &modDate, longLen);
    bufLen += longLen;

    return databuf;
  }

  /* Return the size of the buffer. */
  inline int getBufferSize() { return bufLen; }   

  /* Print out all data members. */
  void show() {
    printf("Region ID: %ld\n", ID);
    printf("Region namespace: %s\n", name_space.c_str());
    printf("Region name: %s\n", name.c_str());
    printf("Region abbreviation: %s\n", abbrev.c_str());
    printf("Original ID in source: %ld\n", orgID);
    printf("Region source: %s\n", source.c_str());
    printf("private flag: %s\n", pFlag.c_str());
    printf("Region link: %s\n", link.c_str());
    printf("Added by: %s\n", creator.c_str());
    if (addDate)
      printf("Region added on: %s", ctime(&addDate));
    else
      printf("Region added on:\n");

    printf("Modified by: %s\n", modifier.c_str());
    if (modDate)
      printf("Last modified on: %s", ctime(&modDate));
    else
      printf("Last modified on:\n");
  }

  inline void setID(long inputVal) { ID = inputVal; }
  inline void setNameSpace(string inputStr) { name_space = inputStr; }
  inline void setName(string inputStr) { name = inputStr; }
  inline void setAbbrev(string inputStr) { abbrev = inputStr; }
  inline void setOrgID(long inputVal) { orgID = inputVal; }
  inline void setSource(string inputStr) { source = inputStr; }
  inline void setPrivate(string inputStr) { pFlag = inputStr; }
  inline void setLink(string inputStr) { link = inputStr; }
  inline void setCreator(string inputStr) { creator = inputStr; }
  inline void setAddDate(long inputVal) { addDate = inputVal; }
  inline void setModifier(string inputStr) { modifier = inputStr; }
  inline void setModDate(long inputVal) { modDate = inputVal; }

  inline long getID() { return ID; }
  inline string getNameSpace() { return name_space; }
  inline string getName() { return name; }
  inline string getAbbrev() { return abbrev; }
  inline long getOrgID() { return orgID; }
  inline string getSource() { return source; }
  inline string getPrivate() { return pFlag; }
  inline string getLink() { return link; }
  inline string getCreator() { return creator; }
  inline long getAddDate() { return addDate; }
  inline string getModifier() { return modifier; }
  inline long getModDate() { return modDate; }

 private:
  /* Utility function that appends a char * to the end of the buffer. */
  void packString(char *buffer, string &theString) {
    int string_size = theString.size() + 1;
    memcpy(buffer + bufLen, theString.c_str(), string_size);
    bufLen += string_size;
  }

  long ID;
  string name_space, name, abbrev;
  long orgID; // original ID from input file, such as ID field in NN2002's hierarchy table
  string source, pFlag, link;
  string creator, modifier;
  long addDate, modDate;

  int bufLen;
  char databuf[1000];
};
