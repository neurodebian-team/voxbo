
// myDB.h
// Copyright (c) 1998-2010 by The VoxBo Development Team

// This file is part of VoxBo
// 
// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
// 
// original version written by Dongbo Hu

/* myDB.h: simple wrapper of the basic _db class */

#ifndef MYDB_H
#define MYDB_H

#include <string>
#include <db_cxx.h>

class myDB
{
 public:
  // Constructor requires a path to the database and a database name.
  myDB(std::string &path, std::string &dbName, u_int32_t openFlag = DB_CREATE,
       bool idCompare = true, bool allowDup = false, DBTYPE dbtype = DB_BTREE);
  // Destructor just calls private close method.
  ~myDB() { close(); }

  inline Db &getDb() { return db_; }
  long countRec();


 private:
  Db db_;
  std::string dbFileName_;
  u_int32_t cFlags_;

  // Make sure the default constructor is private
  myDB() : db_(NULL, 0) { }
    
  // put our database close activity here. Called from our destructor. Could be public
  void close();

};

int compare_int(DB *dbp, const DBT *dbt1, const DBT *dbt2);

#endif
