/********************************************************************************
** Form generated from reading ui file 'browseRegion.ui'
**
** Created: Thu Oct 7 12:03:25 2010
**      by: Qt User Interface Compiler version 4.5.3
**
** WARNING! All changes made in this file will be lost when recompiling ui file!
********************************************************************************/

#ifndef UI_BROWSEREGION_H
#define UI_BROWSEREGION_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QComboBox>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QListWidget>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_brainBrowser
{
public:
    QAction *aboutAct;
    QWidget *centralwidget;
    QLabel *label_6;
    QLabel *label_2;
    QLabel *label_5;
    QPushButton *closeButt;
    QLabel *label_3;
    QLabel *name_title;
    QListWidget *synList;
    QLabel *label_4;
    QLabel *label_9;
    QLineEdit *linkEdit;
    QListWidget *relationList;
    QListWidget *parent;
    QLineEdit *srcEdit;
    QLabel *label_8;
    QLineEdit *nameEdit;
    QLabel *label;
    QLabel *main_title;
    QListWidget *child;
    QComboBox *name_space;
    QListWidget *searchList;
    QPushButton *resetButt;
    QMenuBar *menubar;
    QMenu *menuHelp;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *brainBrowser)
    {
        if (brainBrowser->objectName().isEmpty())
            brainBrowser->setObjectName(QString::fromUtf8("brainBrowser"));
        brainBrowser->resize(620, 604);
        aboutAct = new QAction(brainBrowser);
        aboutAct->setObjectName(QString::fromUtf8("aboutAct"));
        QFont font;
        font.setStyleStrategy(QFont::PreferAntialias);
        aboutAct->setFont(font);
        centralwidget = new QWidget(brainBrowser);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        label_6 = new QLabel(centralwidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(60, 346, 0, 17));
        label_6->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(80, 454, 101, 20));
        label_2->setFont(font);
        label_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_5 = new QLabel(centralwidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(39, 186, 141, 20));
        label_5->setFont(font);
        label_5->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        closeButt = new QPushButton(centralwidget);
        closeButt->setObjectName(QString::fromUtf8("closeButt"));
        closeButt->setGeometry(QRect(360, 530, 91, 28));
        closeButt->setFont(font);
        label_3 = new QLabel(centralwidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(90, 286, 91, 20));
        label_3->setFont(font);
        label_3->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        name_title = new QLabel(centralwidget);
        name_title->setObjectName(QString::fromUtf8("name_title"));
        name_title->setGeometry(QRect(10, 57, 171, 20));
        QFont font1;
        font1.setFamily(QString::fromUtf8("Helvetica"));
        font1.setStyleStrategy(QFont::PreferAntialias);
        name_title->setFont(font1);
        name_title->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        synList = new QListWidget(centralwidget);
        synList->setObjectName(QString::fromUtf8("synList"));
        synList->setGeometry(QRect(190, 246, 381, 91));
        label_4 = new QLabel(centralwidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(50, 120, 131, 20));
        label_4->setFont(font);
        label_4->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        label_9 = new QLabel(centralwidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(130, 383, 51, 20));
        label_9->setFont(font);
        label_9->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        linkEdit = new QLineEdit(centralwidget);
        linkEdit->setObjectName(QString::fromUtf8("linkEdit"));
        linkEdit->setGeometry(QRect(190, 380, 381, 27));
        linkEdit->setFont(font);
        linkEdit->setReadOnly(true);
        relationList = new QListWidget(centralwidget);
        relationList->setObjectName(QString::fromUtf8("relationList"));
        relationList->setGeometry(QRect(190, 415, 381, 101));
        relationList->setFont(font);
        parent = new QListWidget(centralwidget);
        parent->setObjectName(QString::fromUtf8("parent"));
        parent->setGeometry(QRect(190, 116, 381, 25));
        parent->setFont(font);
        parent->setFrameShape(QFrame::StyledPanel);
        parent->setFrameShadow(QFrame::Sunken);
        parent->setLineWidth(1);
        srcEdit = new QLineEdit(centralwidget);
        srcEdit->setObjectName(QString::fromUtf8("srcEdit"));
        srcEdit->setGeometry(QRect(190, 345, 381, 27));
        srcEdit->setFont(font);
        srcEdit->setReadOnly(true);
        label_8 = new QLabel(centralwidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(110, 350, 71, 20));
        label_8->setFont(font);
        label_8->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        nameEdit = new QLineEdit(centralwidget);
        nameEdit->setObjectName(QString::fromUtf8("nameEdit"));
        nameEdit->setGeometry(QRect(190, 50, 381, 29));
        nameEdit->setFont(font);
        label = new QLabel(centralwidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(100, 90, 81, 20));
        label->setFont(font);
        label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        main_title = new QLabel(centralwidget);
        main_title->setObjectName(QString::fromUtf8("main_title"));
        main_title->setGeometry(QRect(210, 10, 221, 21));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        font2.setStyleStrategy(QFont::PreferAntialias);
        main_title->setFont(font2);
        main_title->setAlignment(Qt::AlignCenter);
        child = new QListWidget(centralwidget);
        child->setObjectName(QString::fromUtf8("child"));
        child->setGeometry(QRect(190, 147, 381, 91));
        child->setFont(font);
        name_space = new QComboBox(centralwidget);
        name_space->setObjectName(QString::fromUtf8("name_space"));
        name_space->setGeometry(QRect(190, 83, 141, 28));
        name_space->setFont(font);
        searchList = new QListWidget(centralwidget);
        searchList->setObjectName(QString::fromUtf8("searchList"));
        searchList->setEnabled(true);
        searchList->setGeometry(QRect(190, 77, 381, 6));
        searchList->setFont(font);
        resetButt = new QPushButton(centralwidget);
        resetButt->setObjectName(QString::fromUtf8("resetButt"));
        resetButt->setGeometry(QRect(220, 530, 91, 28));
        resetButt->setFont(font);
        brainBrowser->setCentralWidget(centralwidget);
        menubar = new QMenuBar(brainBrowser);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 620, 23));
        menuHelp = new QMenu(menubar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        brainBrowser->setMenuBar(menubar);
        statusbar = new QStatusBar(brainBrowser);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        brainBrowser->setStatusBar(statusbar);
        QWidget::setTabOrder(nameEdit, searchList);
        QWidget::setTabOrder(searchList, name_space);
        QWidget::setTabOrder(name_space, parent);
        QWidget::setTabOrder(parent, child);
        QWidget::setTabOrder(child, synList);
        QWidget::setTabOrder(synList, srcEdit);
        QWidget::setTabOrder(srcEdit, linkEdit);
        QWidget::setTabOrder(linkEdit, relationList);
        QWidget::setTabOrder(relationList, closeButt);

        menubar->addAction(menuHelp->menuAction());
        menuHelp->addAction(aboutAct);

        retranslateUi(brainBrowser);

        QMetaObject::connectSlotsByName(brainBrowser);
    } // setupUi

    void retranslateUi(QMainWindow *brainBrowser)
    {
        brainBrowser->setWindowTitle(QApplication::translate("brainBrowser", "Brain Region Browser", 0, QApplication::UnicodeUTF8));
        aboutAct->setText(QApplication::translate("brainBrowser", "&About", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("brainBrowser", "Added on:", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("brainBrowser", "Relationship:", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("brainBrowser", "Child Structure(s):", 0, QApplication::UnicodeUTF8));
        closeButt->setText(QApplication::translate("brainBrowser", "Close", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("brainBrowser", "Synonyms:", 0, QApplication::UnicodeUTF8));
        name_title->setText(QApplication::translate("brainBrowser", "Search Region Name:", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("brainBrowser", "Parent Structure:", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("brainBrowser", "Link:", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("brainBrowser", "Source:", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("brainBrowser", "Located in:", 0, QApplication::UnicodeUTF8));
        main_title->setText(QApplication::translate("brainBrowser", "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'Helvetica'; font-size:12pt; font-weight:600; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'Sans Serif';\"><span style=\" font-size:13pt;\">Brain Region Browser</span></p></body></html>", 0, QApplication::UnicodeUTF8));
        name_space->clear();
        name_space->insertItems(0, QStringList()
         << QApplication::translate("brainBrowser", "All namespaces", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("brainBrowser", "NN2002", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("brainBrowser", "AAL", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("brainBrowser", "Brodmann", 0, QApplication::UnicodeUTF8)
        );
        resetButt->setText(QApplication::translate("brainBrowser", "Reset", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("brainBrowser", "&Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class brainBrowser: public Ui_brainBrowser {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_BROWSEREGION_H
