
// synonym.h
// Copyright (c) 2010 by The VoxBo Development Team

// This file is part of VoxBo
// 
// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
// 
// original version written by Dongbo Hu

/***********************************************************************************************
 * synonymRec class definition: hold brain region synonym information.
 * It may seem to make more sense to use primary ID to replace "primary" and "name_space". 
 * But the combination of "priomary" and "name_space" fields will speed up search functionality
 * on the interface. If region ID is used, when a certain string is searched, region name table 
 * will have to be opened to get namespace information, which makes brain region real time search 
 * very slow (takes about 55 seconds to launch the QT interface of "brainRegion").
 ************************************************************************************************/

using namespace std;

#include <string>
#include <string.h>
#include <time.h>

class synonymRec
{
 public:
  synonymRec() { clear(); }

  synonymRec(void *buffer) {
    char *buf = (char *) buffer;

    ID = *((long *) buf);
    bufLen = sizeof(long);

    name = buf + bufLen;
    bufLen += name.size() + 1;

    primary = buf + bufLen;
    bufLen += primary.size() + 1;

    name_space = buf + bufLen;
    bufLen += name_space.size() + 1;

    sourceID = *((long *)(buf + bufLen));
    bufLen += sizeof(long);

    qualifier = buf + bufLen;
    bufLen += qualifier.size() + 1;

    creator = buf + bufLen;
    bufLen += creator.size() + 1;

    addDate = *((long *)(buf + bufLen));
    bufLen += sizeof(long);

    modifier = buf + bufLen;
    bufLen += modifier.size() + 1;

    modDate = *((long *)(buf + bufLen));
    bufLen += sizeof(long);

    comments = buf + bufLen;
    bufLen += comments.size() + 1;
  }

  /* Initialize data members. */
  void clear() {
    ID = 0;
    name = "";
    primary = name_space = "";
    sourceID = 0;
    qualifier = "";
    creator = modifier = "";
    addDate = modDate = 0;
    comments = "";
  }

  /* Return combined data members as a single contiguous memory location. */
  char * getBuffer() {
    memset(databuf, 0, 1000);
    bufLen = 0;
    int longLen = sizeof(long);

    memcpy(databuf, &ID, longLen);
    bufLen += longLen;

    packString(databuf, name);
    packString(databuf, primary);
    packString(databuf, name_space);

    memcpy(databuf + bufLen, &sourceID, longLen);
    bufLen += longLen;

    packString(databuf, qualifier);

    packString(databuf, creator);
    memcpy(databuf + bufLen, &addDate, longLen);
    bufLen += longLen;

    packString(databuf, modifier);
    memcpy(databuf + bufLen, &modDate, longLen);
    bufLen += longLen;

    packString(databuf, comments);

    return databuf;
  }

  /* Return the size of the buffer. */
  inline int getBufferSize() { return bufLen; }

  void show() {
    printf("Synonym ID: %ld\n", ID);
    printf("Synonym name: %s\n", name.c_str());
    printf("Primary structure: %s\n", primary.c_str());
    printf("Namespace: %s\n", name_space.c_str());

    if (sourceID)
      printf("ID in source file: %ld\n", sourceID);
    else
      printf("ID in source file:\n");

    printf("Qualifier: %s\n", qualifier.c_str());

    printf("Added by: %s\n", creator.c_str());
    if (addDate)
      printf("Added on: %s", ctime(&addDate));
    else
      printf("Added on:\n");

    printf("Modified by: %s\n", modifier.c_str());
    if (modDate)
      printf("Last modified on: %s", ctime(&modDate));
    else
      printf("Last modified on:\n");

    printf("Comments: %s\n", comments.c_str());
  }

  inline void setID(long inputVal) { ID = inputVal; }
  inline void setName(string inputStr) { name = inputStr; }
  inline void setPrimary(string inputStr) { primary = inputStr; }
  inline void setNameSpace(string inputStr) { name_space = inputStr; }
  inline void setSourceID(long inputVal) { sourceID = inputVal; }
  inline void setQualifier(string inputStr) { qualifier = inputStr; }
  inline void setCreator(string inputStr) { creator = inputStr; }
  inline void setAddDate(long inputVal) { addDate = inputVal; }
  inline void setModifier(string inputStr) { modifier = inputStr; }
  inline void setModDate(long inputVal) { modDate = inputVal; }
  inline void setComments(string inputStr) { comments = inputStr; }

  inline long getID() { return ID; }
  inline string getName() { return name; }
  inline string getPrimary() { return primary; }
  inline string getNameSpace() { return name_space; }
  inline long getSourceID() { return sourceID; }
  inline string getQualifier() { return qualifier; }
  inline string getCreator() { return creator; }
  inline long getAddDate() { return addDate; }
  inline string getModifier() { return modifier; }
  inline long getModDate() { return modDate; }
  inline string getComments() { return comments; }

 private:
  /* Utility function that appends a char * to the end of the buffer. */
  void packString(char *buffer, string &theString) {
    int string_size = theString.size() + 1;
    memcpy(buffer + bufLen, theString.c_str(), string_size);
    bufLen += string_size;
  }
  
  long ID, sourceID;
  string name, primary, name_space;
  string qualifier;
  string creator, modifier;
  long addDate, modDate;
  string comments;

  int bufLen;
  char databuf[1000];
};
