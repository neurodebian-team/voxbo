
// main.cpp
// Copyright (c) 1998-2010 by The VoxBo Development Team

// This file is part of VoxBo
// 
// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
// 
// original version written by Dongbo Hu

/* Possible bug: 
 * when adding new relationship on "add new structure" interface,
 * the new relationship's compatibility is not checked completely
 * with other relationships. 
 * For example, suppose the new structure is A, B and C already exists 
 * and B is part of (or child of) C, user is allowed to added:
 * A is part of B;
 * A includes C;
 * although they should be incompatible. 
 */
 
#include "main.h"
#include "main.moc.h"
#include <QApplication>
#include <QCompleter>
#include <QFileInfo>
#include <QInputDialog>
#include <QMessageBox>

int main(int argc, char *argv[]) {
  string dbHome;
  string rDbName = "region_name.db";
  string rrDbName = "region_relation.db";
  string sDbName = "synonym.db";
  if (!setFiles(rDbName, rrDbName, sDbName, dbHome)) {
    printf("Database files not found in the following directories:\n");
    printf("  /usr/share/brainregions/\n");
    printf("  /usr/local/brainregions/\n");
    printf("  $HOME/brainregions/\n");
    printf("  ./\n");
    printf("Application aborted.\n");
    exit(0);
  }

  QApplication app(argc, argv);
  browser *myWindow = new browser(dbHome, rDbName, rrDbName, sDbName);
  myWindow->show();

  return app.exec();
}

/* Set location of bdb files. */
bool setFiles(string rDbName, string rrDbName, string sDbName, string &dbHome)
{
  QStringList dbDirs;
  dbDirs << "/usr/share/brainregions/" << "/usr/local/brainregions/";
  char *homedir = getenv("HOME");

  if (homedir)
    dbDirs.append(QString(homedir) + "/brainregions/");

  dbDirs.append("./");

  int i;
  for (i = 0; i < dbDirs.count(); i++) {
    QFileInfo fi1 = dbDirs[i] + QString::fromStdString(rDbName);
    QFileInfo fi2 = dbDirs[i] + QString::fromStdString(rrDbName);
    QFileInfo fi3 = dbDirs[i] + QString::fromStdString(sDbName);

    if (fi1.isReadable() && fi2.isReadable() && fi3.isReadable()) {
      dbHome = dbDirs[i].toStdString();
      break;
    }
  }

  if (i < dbDirs.count())
    return true;

  return false;
}

/* Simple constructor */
browser::browser(string home_in, string rdb_in, string rrdb_in, string sdb_in, QWidget *parent) 
  : QMainWindow(parent) 
{
  dbHome = home_in;
  rDbName = rdb_in;
  rrDbName = rrdb_in;
  sDbName = sdb_in;

  regionName = "";
  buildUI();
}

/* build the main interface */
void browser::buildUI()
{
  ui_browser.setupUi(this);
  ui_browser.searchList->hide();
  buildList();
  
  ui_browser.nameEdit->setFocus();
  connect(ui_browser.nameEdit, SIGNAL(textEdited(const QString &)), this, SLOT(popupList()));
  connect(ui_browser.searchList, SIGNAL(itemActivated(QListWidgetItem *)), 
	  this, SLOT(selectName(QListWidgetItem *)));
  connect(ui_browser.name_space, SIGNAL(currentIndexChanged(int)), this, SLOT(changeNS()));
  connect(ui_browser.parent, SIGNAL(itemDoubleClicked(QListWidgetItem *)), 
	  this, SLOT(toParent(QListWidgetItem *)));
  connect(ui_browser.child, SIGNAL(itemDoubleClicked(QListWidgetItem *)), 
	  this, SLOT(toChild(QListWidgetItem *)));  
  connect(ui_browser.relationList, SIGNAL(itemDoubleClicked(QListWidgetItem *)), 
	  this, SLOT(toRelated(QListWidgetItem *)));  

  connect(ui_browser.aboutAct, SIGNAL(triggered()), this, SLOT(about()));
  connect(ui_browser.resetButt, SIGNAL(clicked()), this, SLOT(resetUI()));
  connect(ui_browser.closeButt, SIGNAL(clicked()), this, SLOT(close()));

  setWindowTitle("Browse Database in " + QString::fromStdString(dbHome));
}

/* Add structure names into wordList */
void browser::buildList()
{
  nameList.clear();
  spaceList.clear();

  string currentSpace = ui_browser.name_space->currentText().toStdString();
  int foo;
  if (currentSpace == "All namespaces")
    foo = getAllRegions(dbHome, rDbName, nameList, spaceList);
  else
    foo = getRegions(dbHome, rDbName, currentSpace, nameList);

  if (foo) {
    QMessageBox::critical(0, "Error", "Region name db exception.");
    return;
  }

  if (currentSpace == "All namespaces")
    foo = getAllSynonyms(dbHome, sDbName, nameList, spaceList);
  else 
    foo = getSynonyms(dbHome, sDbName, currentSpace, nameList);

  if (foo) {
    QMessageBox::critical(0, "Error", "Synonym db exception.");
    return;
  }

}

/* This slot replies to the name_space change signal. 
 * It rebuilds search list and start a new search. */
void browser::changeNS()
{
  buildList();
  clearUI();
  popupList();
}

/* This slot pops up the completion list */
void browser::popupList()
{
  ui_browser.searchList->clear();

  QString searchStr = ui_browser.nameEdit->text().simplified();
  if (!searchStr.length()) {
    clearUI();
    ui_browser.searchList->hide();
    return;
  }

  for (uint i = 0; i < nameList.size(); i++) {
    QString itemStr = nameList[i].c_str();
    if (itemStr.contains(QRegExp(searchStr, Qt::CaseInsensitive))) {
      if (ui_browser.name_space->currentIndex() == 0)
	itemStr = itemStr + " (" + spaceList[i].c_str() + ")";
      ui_browser.searchList->addItem(itemStr);
    }
  }

  if (!ui_browser.searchList->count()) {
    ui_browser.searchList->hide();
    return;
  }

  ui_browser.searchList->setMinimumHeight(165);
  ui_browser.searchList->sortItems(Qt::AscendingOrder);
  ui_browser.searchList->show();
}

/* This slot selects one of the names from list box and show relevant infomation on interface */
void browser::selectName(QListWidgetItem *mySel)
{
  //ui_browser.main_title->setText("Brain Region Browser");
  //ui_browser.name_title->setText("Search Region Name:");

  QString comboStr = mySel->text();
  QString nameStr, spaceStr;
  bool withNS = true;
  if (ui_browser.name_space->currentIndex())
    withNS = false;
  int foo = parseRegionName(comboStr, withNS, nameStr, spaceStr);
  if (foo == 1) {
    QMessageBox::critical(0, "Error", "Invalid selection string: " + comboStr);
    ui_browser.searchList->hide();
    return;
  }
  if (foo == 2) {
    QMessageBox::critical(0, "Error", "Invalid name space: " + spaceStr);
    ui_browser.searchList->hide();
    return;
  }

  ui_browser.nameEdit->setText(nameStr);
  ui_browser.searchList->hide();
  searchRegion(nameStr.toStdString(), spaceStr.toStdString());
  if (regionName.length())
    return;

  searchSynonym(nameStr.toStdString(), spaceStr.toStdString());
  ui_browser.nameEdit->setFocus();
}

/* This function reads the first argument and divides it into two parts: 
 * region name and namespace. 
 * Returns 0 if everything is ok;
 * returns 1 if input string can't be divided successfully.
 * returns 2 if namespace is unknown;
 */
int browser::parseRegionName(QString inputStr, bool withNS, QString & nameStr, QString & spaceStr)
{
  if (!withNS) {
    nameStr = inputStr;
    spaceStr = ui_browser.name_space->currentText();
    return 0;
  }
    
  int strLen = inputStr.length();
  int foo = inputStr.lastIndexOf("(");
  int bar = inputStr.lastIndexOf(")");
  if (foo == -1 || foo < 2 || bar == -1 || foo >= bar || bar != strLen - 1)
    return 1;

  nameStr = inputStr.left(foo - 1);
  spaceStr = inputStr.mid(foo + 1, bar - foo - 1);
  if (!chkNameSpace(spaceStr))
    return 2;

  return 0;
}

/* This function checks whether the input string is a valid namespace. If it is,
 * set the namespace combo box on the interface to correct value and returns true;
 * returns false otherwise. */
bool browser::chkNameSpace(QString inputStr)
{
  if (inputStr == "NN2002")
    return true;

  if (inputStr == "AAL")
    return true;

  if (inputStr == "Brodmann") 
    return true;

  if (inputStr == "Marianna")
    return true;

  if (inputStr == "QT_UI")
    return true;

  return false;
}

/* This function checks whether the input string is a valid namespace. If it is,
 * set the namespace combo box on the interface to correct value and returns true;
 * returns false otherwise. */
void browser::setNameSpace(string inputStr)
{
  int nsIndex;
  if (inputStr == "NN2002")
    nsIndex = 1;
  else if (inputStr == "AAL")
    nsIndex = 2;
  else if (inputStr == "Brodmann")
    nsIndex = 3;
  else {
    QMessageBox::critical(0, "Error", "Unknown namespace: " + QString(inputStr.c_str()));
    return;
  }

  disconnect(ui_browser.name_space, SIGNAL(currentIndexChanged(int)), this, SLOT(changeNS()));
  ui_browser.name_space->setCurrentIndex(nsIndex);
  name_space = inputStr;
  buildList();
  connect(ui_browser.name_space, SIGNAL(currentIndexChanged(int)), this, SLOT(changeNS()));
}

/* seach a structure name in region_name.db and show its information on the interface */
void browser::searchRegion(string rName, string inputSpace)
{
  clearUI();

  regionRec rData;
  int foo = getRegionRec(dbHome, rDbName, rName, inputSpace, rData);
  // quit if db file exception is met
  if (foo < 0) {
    QMessageBox::critical(0, "Error", "Region name db exception");
    return;
  }

  // quit if the name is not found in region name db file
  if (foo == 0)
    return;

  regionID = rData.getID();
  regionName = rName;

  //long add_time = rData.getAddDate();
  //if(add_time)
  //  ui_browser.addDate->setText(ctime(&add_time));

  //long mod_time = rData.getModDate();
  //if (mod_time) 
  //  ui_browser.modDate->setText(ctime(&mod_time));
  
  ui_browser.nameEdit->setText(rName.c_str());
  setNameSpace(inputSpace);
  ui_browser.srcEdit->setText(rData.getSource().c_str());
  ui_browser.linkEdit->setText(rData.getLink().c_str());

  showParentChild();
  showSynonym();
  showRelation();
  //setWindowTitle("Brain Region Info: " + ui_browser.nameEdit->text());  
}

/* seach a synonym name and show relevant information on the interface */
void browser::searchSynonym(string sName, string inputSpace)
{
  string pName;
  int foo = getPrimary(dbHome, sDbName, sName, inputSpace, pName);
  if (foo < 0) {
    QMessageBox::critical(0, "Error", "Synonym db exception.");
    return;
  }

  // quit if the name is not found in synonym db file
  if (foo == 0) {
    QMessageBox::critical(0, "Error", "Name not found in region name and synonym db files: " + 
			  ui_browser.nameEdit->text());
    return;
  }

  searchRegion(pName, inputSpace);
}

/* This functions clears all information on the interface */
void browser::clearUI()
{
  regionID = 0;
  regionName = name_space = "";
  ui_browser.parent->clear();
  ui_browser.child->clear();
  ui_browser.synList->clear();
  //ui_browser.addDate->clear();
  //ui_browser.modDate->clear();
  ui_browser.srcEdit->clear();
  ui_browser.linkEdit->clear();
  ui_browser.relationList->clear();
}

/* This slot responds to "reset button click. */
void browser::resetUI()
{
  ui_browser.nameEdit->clear();
  clearUI();
}

/* Show parent and child information on the interface */
void browser::showParentChild()
{
  long parentID = 0;
  vector<long> cList;

  int foo = getParentChild(dbHome, rrDbName, regionID, &parentID, cList);
  if (foo < 0) {
    QMessageBox::critical(0, "Error", "Relationship db exception.");
    return;
  }

  // If parent exists, get its name and show on interface
  if (parentID) {
    string parentStr, spaceStr;
    foo = getRegionName(dbHome, rDbName, parentID, parentStr, spaceStr);
    if (foo == 1)
      ui_browser.parent->addItem(parentStr.c_str());
    else
      QMessageBox::critical(0, "Error", "Fails to get parent region name from ID " +
			    QString::number(parentID) + " in region db.");
  }

  for (unsigned i = 0; i < cList.size(); i++) {
    long cID = cList[i];
    string cName, spaceStr;
    foo = getRegionName(dbHome, rDbName, cID, cName, spaceStr);
    if (foo == 1)
      ui_browser.child->addItem(cName.c_str());
    else
      QMessageBox::critical(0, "Error", "Fails to get child region name from ID " + 
			    QString::number(cID) + " in region name db.");
  }

  if (ui_browser.child->count())
    ui_browser.child->sortItems(Qt::AscendingOrder);
}

/* This function updates the current parent region name. */
void browser::showParent()
{
  ui_browser.parent->clear();

  long pID = 0;
  int foo = getParent(dbHome, rrDbName, regionID, &pID);
  if (foo < 0) {
    QMessageBox::critical(0, "Error", "Relationship db exception.");
    return;
  }

  if (!pID)
    return;

  string parentStr, spaceStr;
  foo = getRegionName(dbHome, rDbName, pID, parentStr, spaceStr);
  if (foo != 1) {
    QMessageBox::critical(0, "Error", "Fails to get parent region name from ID " +
			  QString::number(pID) + " in region db.");
    return;
  }
  ui_browser.parent->addItem(parentStr.c_str());
}

/* Show parent and child information on the interface */
void browser::showChild()
{
  ui_browser.child->clear();

  vector<long> cList;
  int foo = getChild(dbHome, rrDbName, regionID, cList);
  if (foo < 0) {
    QMessageBox::critical(0, "Error", "Relationship db exception.");
    return;
  }

  for (unsigned i = 0; i < cList.size(); i++) {
    long cID = cList[i];
    string cName, spaceStr;
    foo = getRegionName(dbHome, rDbName, cID, cName, spaceStr);
    if (foo == 1)
      ui_browser.child->addItem(cName.c_str());
    else
      QMessageBox::critical(0, "Error", "Fails to get child region name from ID " + 
			    QString::number(cID) + " in region name db.");
  }

  if (ui_browser.child->count())
    ui_browser.child->sortItems(Qt::AscendingOrder);
}

/* This function shows synonym(s) on the interface */
void browser::showSynonym()
{
  ui_browser.synList->clear();

  vector<string> symList;
  int foo = getSynonym(dbHome, sDbName, regionName, name_space, symList);
  if (foo < 0) {
    QMessageBox::critical(0, "Error", "Synonym db exception.");
    return;
  }

  for (unsigned i = 0; i < symList.size(); i++)
    ui_browser.synList->addItem(symList[i].c_str());
}

/* This function collects input region's relationship information and show on the interface.
 * Note that child/parent relationships are excluded because they are already shown. */
void browser::showRelation()
{
  ui_browser.relationList->clear();

  vector<long> r2List;
  vector<string> relList;
  int foo = getRel_ui(dbHome, rrDbName, regionID, r2List, relList);
  if (foo < 0) {
    QMessageBox::critical(0, "Error", "Relationship db exception.");
    return;
  }

  for (unsigned i = 0; i < relList.size(); i++) {
    string r2_name, r2_space;
    foo = getRegionName(dbHome, rDbName, r2List[i], r2_name, r2_space);
    if (foo == 1) {
      string tmpStr = relList[i] + ": " + r2_name + " (" + r2_space + ")";
      ui_browser.relationList->addItem(tmpStr.c_str());
    }
    else
      QMessageBox::critical(0, "Error", "Fails to get region name from ID " + 
			    QString::number(r2List[i]) + " in region name db.");
  }
}
 
/* This function collects parent structure info and show on the interface */
void browser::toParent(QListWidgetItem *pItem)
{
  QString pStr = pItem->text();
  // Return 0 if parent name is same as the structure name ("BRAIN")
  if (regionName == pStr.toStdString())
    return;

  ui_browser.nameEdit->setText(pStr);
  searchRegion(pStr.toStdString(), name_space);
}

/* This is the slot when item in child name list is double clicked */
void browser::toChild(QListWidgetItem *selItem)
{
  QString selName = selItem->text();
  ui_browser.nameEdit->setText(selName);
  searchRegion(selName.toStdString(), name_space);
}

/* This slot takes care of double click signal in relationship box. */
void browser::toRelated(QListWidgetItem *selItem)
{
  QString relStr = selItem->text();
  int colon_post = relStr.indexOf(": ");
  int ns_post = relStr.lastIndexOf(" (");
  if (colon_post == -1 || ns_post == -1) {
    QMessageBox::critical(0, "Error", "Relationship not understood: " + relStr);
    return;
  }

  QString newRegion = relStr.mid(colon_post + 2, ns_post - colon_post - 2);
  QString newNS =  relStr.mid(ns_post + 2, relStr.length() - ns_post - 3);

  ui_browser.nameEdit->setText(newRegion);
  searchRegion(newRegion.toStdString(), newNS.toStdString());
}

/* Show acknowledgement information. */
void browser::about()
{
  string helptext=
    "This initial release of the VoxBo Brain Structure Browser was written by Dongbo Hu (code) and "
    "Daniel Kimberg (sage advice).  It is distributed along with structure "
    "information derived from the NeuroNames project (Bowden and Dubach, "
    "2002) as well as the AAL atlas (Automatic Anatomical Labeling, "
    "Tzourio-Mazoyer et al., 2002).<p><p>"
    "<b>References:</b><p>"
    "Tzourio-Mazoyer N, Landeau B, Papathanassiou D, Crivello F, Etard O, "
    "Delcroix N, Mazoyer B, Joliot M (2002).  \"Automated Anatomical "
    "Labeling of activations in SPM using a Macroscopic Anatomical "
    "Parcellation of the MNI MRI single-subject brain.\"  NeuroImage 15 (1), "
    "273-89.<p><p>"
    "Bowden D and Dubach M (2003).  NeuroNames 2002.  Neuroinformatics, 1, "
    "43-59.";

  QMessageBox::about(this, tr("About Application"), tr(helptext.c_str()));
}

/* This overloaded function takes care of some key press events */
void browser::keyPressEvent(QKeyEvent * kEvent)  
{
  if (ui_browser.nameEdit->hasFocus() || ui_browser.searchList->hasFocus()) {
    if (ui_browser.searchList->isVisible() && kEvent->key() == Qt::Key_Escape) {
      ui_browser.searchList->setFocus();
      ui_browser.searchList->hide();
      ui_browser.nameEdit->setFocus();
    }
  }
  if (ui_browser.nameEdit->hasFocus() && ui_browser.searchList->count() 
      && ui_browser.searchList->isVisible()) {
    if (kEvent->key() == Qt::Key_Down) {
      ui_browser.searchList->setFocus();
      ui_browser.searchList->setCurrentRow(0);
    }
    else if (kEvent->key() == Qt::Key_Up) {
      ui_browser.searchList->setFocus();
      ui_browser.searchList->setCurrentRow(ui_browser.searchList->count() - 1);
    }
    else
      ui_browser.searchList->show();
  }
  else if (ui_browser.searchList->hasFocus() && 
	   ui_browser.searchList->currentRow() == ui_browser.searchList->count() - 1 && 
	   kEvent->key() == Qt::Key_Down) 
    ui_browser.searchList->setCurrentRow(0);
  else if (ui_browser.searchList->hasFocus() && 
	   ui_browser.searchList->currentRow() == 0 && kEvent->key() == Qt::Key_Up) 
    ui_browser.searchList->setCurrentRow(ui_browser.searchList->count() - 1);
  else if (ui_browser.searchList->hasFocus() && kEvent->key() == Qt::Key_Backspace) {
    ui_browser.nameEdit->setFocus();
    ui_browser.nameEdit->backspace();
  }
  else if (ui_browser.searchList->hasFocus() && kEvent->key() == Qt::Key_Delete) {
    ui_browser.nameEdit->setFocus();
    ui_browser.nameEdit->del();
  }
  else if (ui_browser.searchList->hasFocus() && kEvent->key() != Qt::Key_Up
	   && kEvent->key() != Qt::Key_Down && kEvent->key() != Qt::Key_Return && kEvent->key() != Qt::Key_Enter
	   && kEvent->key() != Qt::Key_PageUp && kEvent->key() != Qt::Key_PageDown) {
    ui_browser.nameEdit->setFocus();
    QString orgStr = ui_browser.nameEdit->text();
    QString newStr = kEvent->text();
    if (newStr.length()) {
      ui_browser.nameEdit->setText(orgStr + newStr);
      popupList();
    }
  }
  else if (!ui_browser.nameEdit->hasFocus() && !ui_browser.searchList->hasFocus())
    ui_browser.searchList->hide();
 
  //kEvent->accept();
  QMainWindow::keyPressEvent(kEvent);
}


