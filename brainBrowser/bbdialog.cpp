
// bbdialog.cpp
// dialogs for structure browser
// Copyright (c) 2010 by The VoxBo Development Team

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,
// version 3, as published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file named COPYING.  If not, write
// to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
// Boston, MA 02111-1307 USA
//
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
//
// original version written by Dan Kimberg, based on a design by
// Dongbo Hu.

#include "bbdialog.moc.h"

BBdialog::BBdialog(QWidget *parent)
  : QDialog(parent)
{
  // main layout
  QVBoxLayout *layout=new QVBoxLayout();
  layout->setAlignment(Qt::AlignTop);
  layout->setSpacing(2);
  layout->setMargin(4);
  this->setLayout(layout);

  // form layout
  QFormLayout *myform=new QFormLayout();
  layout->addLayout(myform);

  // stuff in form layout
  w_name=new QLineEdit();
  w_namespace=new QComboBox();
  w_parent=new QListWidget();
  w_child=new QListWidget();
  w_synonyms=new QListWidget();
  w_source=new QLineEdit();
  w_link=new QLineEdit();
  w_relationship=new QListWidget();
  myform->addRow("Region Name:",w_name);
  myform->addRow("Located in:",w_namespace);
  myform->addRow("Parent Structure:",w_parent);
  myform->addRow("Child Structure(s):",w_child);
  myform->addRow("Synonyms:",w_synonyms);
  myform->addRow("Source:",w_source);
  myform->addRow("Link:",w_link);
  myform->addRow("Relationship:",w_relationship);

  hb=new QHBox();
  layout->addWidget(hb);
  button=new QPushButton("Reset");
  hb->addWidget(button);
  QObject::connect(button,SIGNAL(clicked()),this,SLOT(accept()));
  button=new QPushButton("Close");
  hb->addWidget(button);
  QObject::connect(button,SIGNAL(clicked()),this,SLOT(reject()));
  // setMinimumSize(320,1);
  setWindowTitle("VoxBo Brain Structure Name Browser");
}
