
// main.h
// Copyright (c) 1998-2010 by The VoxBo Development Team

// This file is part of VoxBo
// 
// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
// 
// original version written by Dongbo Hu

/* This interface allows user to browse/search brain region database. */

#include "br_util.h"
#include "ui_browseRegion.h"
#include <QKeyEvent>

bool setFiles(string, string, string, string &);

class browser : public QMainWindow
{
  Q_OBJECT

public:
  browser(string, string, string, string, QWidget *parent = 0);
  
private:
  void buildUI();
  void buildList();
  int parseRegionName(QString, bool, QString &, QString &);
  bool chkNameSpace(QString);
  void setNameSpace(string);
  void searchRegion(string, string);
  void searchSynonym(string, string);
  void clearUI();

  void showParentChild();
  void showParent();
  void showChild();

  void showSynonym();
  bool chkName(string);
  void showRelation();

  void keyPressEvent(QKeyEvent *);

  string dbHome, rDbName, rrDbName, sDbName;
  Ui_brainBrowser ui_browser;

  vector <string> nameList;
  vector <string> spaceList;
  long regionID;
  string regionName, name_space;

private slots:
  void popupList();
  void selectName(QListWidgetItem *);
  void changeNS();
  void toParent(QListWidgetItem *);
  void toChild(QListWidgetItem *);
  void toRelated(QListWidgetItem *);
  void resetUI();
  void about();
};

