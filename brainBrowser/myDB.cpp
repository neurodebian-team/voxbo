
// myDB.cpp
// Copyright (c) 2010 by The VoxBo Development Team

// This file is part of VoxBo
// 
// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
// 
// original version written by Dongbo Hu

/* Simple database class, more like a wraaper of Berkeley DB's db_ */
using namespace std;

#include "myDB.h"

/* Class constructor. Requires a path to the location
 * where the database is located, and a database name. */
myDB::myDB(std::string &path, std::string &dbName, u_int32_t openFlag, 
	   bool idCompare, bool allowDup, DBTYPE inputType)
  : db_(NULL, 0),               // Instantiate Db object
    dbFileName_(path + dbName)  // Database file name
{
  try {
    // Redirect debugging information to std::cerr
    db_.set_error_stream(&std::cerr);
    // Set encrypt
    db_.set_encrypt("fyeo2006", DB_ENCRYPT_AES);
    // If this is a secondary database, support sorted duplicates
    if (allowDup)
      db_.set_flags(DB_DUPSORT);
    else if (idCompare && inputType == DB_BTREE)
      db_.set_bt_compare(compare_int);

    // Open the database
    db_.open(NULL, dbFileName_.c_str(), NULL, inputType, openFlag, 0);
  }
  // DbException is not a subclass of std::exception, so we need to catch them both.
  catch(DbException &e) {
    std::cerr << "Error opening database: " << dbFileName_ << "\n";
    std::cerr << e.what() << std::endl;
  }
  catch(std::exception &e) {
    std::cerr << "Error opening database: " << dbFileName_ << "\n";
    std::cerr << e.what() << std::endl;
  }
}

/* Private member used to close a database. Called from the class destructor. */
void myDB::close()
{
  // Close the db
  try {
    db_.close(0);
    //std::cout << "Database " << dbFileName_ << " is closed." << std::endl;
  }
  catch(DbException &e) {
    std::cerr << "Error closing database: " << dbFileName_ << "\n";
    std::cerr << e.what() << std::endl;
  }
  catch(std::exception &e) {
    std::cerr << "Error closing database: " << dbFileName_ << "\n";
    std::cerr << e.what() << std::endl;
  }
}

/* This function returns the number of records in database, 
 * used to generate keys automatically. */
long myDB::countRec()
{
  long length = 0;
  // Get a cursor to the patient db
  Dbc *cursorp;
  try {
    getDb().cursor(NULL, &cursorp, 0);
    Dbt key, data;
    while (cursorp->get(&key, &data, DB_NEXT) == 0)
      length++;
  } 
  catch(DbException &e) {
    getDb().err(e.get_errno(), "Error in reading records");
    cursorp->close();
    throw e;
  } 
  catch(exception &e) {
    cursorp->close();
    throw e;
  }

  cursorp->close();
  return length;
}

/* This function compares keys arithmetically instead of lexically.
 * It is called when the db key is a long integer and we want to 
 * rank the records according to the key value. */
int compare_int(DB *dbp, const DBT *dbt1, const DBT *dbt2)
{
  long l = *(long *) dbt1->data;
  long r = *(long *) dbt2->data;

  return l - r;
}

