
// bbdialog.h
// dialogs for structure browser
// Copyright (c) 2010 by The VoxBo Development Team

// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License,
// version 3, as published by the Free Software Foundation.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file named COPYING.  If not, write
// to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
// Boston, MA 02111-1307 USA
//
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
//
// original version written by Dan Kimberg, based on a design by
// Dongbo Hu.

#include <QPushButton>
#include <QStringList>
#include <QFormLayout>

#include "bbdialogs.moc.h"
#include "myboxes.h"

using namespace std;

class BBdialog : public QDialog {
  Q_OBJECT
public:
  BBdialog(QWidget *parent);
public slots:
private:
  QLineEdit *w_name;
  QComboBox *w_namespace;
  QListWidget *w_parent;
  QListWidget *w_child;
  QListWidget *w_synonyms;
  QLineEdit *w_source;
  QLineEdit *w_link;
  QListWidget *w_relationship;
signals:
};
