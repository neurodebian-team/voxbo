
// region_relation.h
// Copyright (c) 2010 by The VoxBo Development Team

// This file is part of VoxBo
// 
// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
// 
// original version written by Dongbo Hu

/*******************************************************************************
 * regionRelationRec class definition: hold region relationship information
 *******************************************************************************/
using namespace std;

#include <string>
#include <string.h>
#include <time.h>

class regionRelationRec
{
 public:
  regionRelationRec() { clear(); }

  regionRelationRec(void *buffer) {
    char *buf = (char *) buffer;
    int longLen = sizeof(long);

    ID = *((long *) buf);
    bufLen = longLen;
    
    region1 = *((long *) (buf + bufLen));
    bufLen += longLen;

    region2 = *((long *) (buf + bufLen));
    bufLen += longLen;

    relationship = buf + bufLen;
    bufLen += relationship.size() + 1;

    qualifier = buf + bufLen;
    bufLen += qualifier.size() + 1;
    
    creator = buf + bufLen;
    bufLen += creator.size() + 1;

    addDate = *((long *) (buf + bufLen));
    bufLen += sizeof(long);

    modifier = buf + bufLen;
    bufLen += modifier.size() + 1;

    modDate = *((long *) (buf + bufLen));
    bufLen += sizeof(long);

    comments = buf + bufLen;
    bufLen += comments.size() + 1;
  }

  /* Initialize data members. */
  void clear() {
    ID = 0;
    region1 = region2 = 0;
    relationship = qualifier = "";
    creator = modifier = "";
    addDate = modDate = 0;
    comments = "";
  }

  /* Return the whole data structure. */
  char * getBuffer() {
    memset(databuf, 0, 1000);
    bufLen = 0;
    int longLen = sizeof(long);

    memcpy(databuf, &ID, longLen);
    bufLen += longLen;

    memcpy(databuf + bufLen, &region1, longLen);
    bufLen += longLen;

    memcpy(databuf + bufLen, &region2, longLen);
    bufLen += longLen;

    packString(databuf, relationship);
    packString(databuf, qualifier);

    packString(databuf, creator);
    memcpy(databuf + bufLen, &addDate, longLen);
    bufLen += longLen;

    packString(databuf, modifier);
    memcpy(databuf + bufLen, &modDate, longLen);
    bufLen += longLen;

    packString(databuf, comments);

    return databuf;
  }

  /* Return the size of the buffer. Used for storing. */
  inline int getBufferSize() { return bufLen; }

  /* Print out all data members. */
  void show() {
    printf("Relation ID: %ld\n", ID);
    printf("Region 1 ID: %ld\n", region1);
    printf("Region 2 ID: %ld\n", region2);
    printf("Relationship: %s\n", relationship.c_str());
    printf("Qualifier: %s\n", qualifier.c_str());

    printf("Added by: %s\n", creator.c_str());
    if (addDate)
      printf("Relation added on: %s", ctime(&addDate));
    else
      printf("Relation added on:\n");

    printf("Modified by: %s\n", modifier.c_str());
    if (modDate)
      printf("Last modified on: %s", ctime(&modDate));
    else
      printf("Last modified on:\n");

    printf("Comments: %s\n", comments.c_str());
  }

  inline void setID(long inputVal) { ID = inputVal; }
  inline void setRegion1(long inputVal) { region1 = inputVal; }
  inline void setRegion2(long inputVal) { region2 = inputVal; }
  inline void setRelationship(string inputStr) { relationship = inputStr; }
  inline void setQualifier(string inputStr) { qualifier = inputStr; }
  inline void setCreator(string inputStr) { creator = inputStr; }
  inline void setAddDate(long inputVal) { addDate = inputVal; }
  inline void setModifier(string inputStr) { modifier = inputStr; }
  inline void setModDate(long inputVal) { modDate = inputVal; }
  inline void setComments(string inputStr) { comments = inputStr; }

  inline long getID() { return ID; }
  inline long getRegion1() { return region1; }
  inline long getRegion2() { return region2; }
  inline string getRelationship() { return relationship; }
  inline string getQualifier() { return qualifier; }
  inline string getCreator() { return creator; }
  inline long getAddDate() { return addDate; }
  inline string getModifier() { return modifier; }
  inline long getModDate() { return modDate; }
  inline string getComments() { return comments; }

 private:
  /* Utility function that appends a char * to the end of the buffer. */
  void packString(char *buffer, string &theString) {
    int string_size = theString.size() + 1;
    memcpy(buffer + bufLen, theString.c_str(), string_size);
    bufLen += string_size;
  }

  long ID, region1, region2;
  string relationship, qualifier;
  string creator, modifier;
  long addDate, modDate;
  string comments;

  int bufLen;
  char databuf[1000];
};
