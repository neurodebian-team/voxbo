
// vbscoregen.cpp
// simulate scores from a mask
// Copyright (c) 2011 by The VoxBo Development Team

// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
//
// original version written by Dan Kimberg

using namespace std;

#include <fstream>
#include "vbutil.h"
#include "vbio.h"
#include "vbscoregen.hlp.h"

void vbscoregen_help();
void vbscoregen_version();


class VBscoregen {
public:
  VBscoregen(string cfg) {parseconfig(cfg);}
private:
  Tes lesions;
  string lesionfile;
  map<string,VBRegion> maskmap;
  map<string,VBVoxel> voxelmap;
  double mean1,sd1;
  double mean2,sd2;
  double floor,ceiling;
  VB_Vector scores;
  gsl_rng *rng;
  uint32 seed;
  void parseconfig(string configfile);
  double genscore(double pct);
  void do_ifvoxel(string v1);
  void do_ifvoxelor(string v1,string v2);
  void do_ifvoxeland(string v1,string v2);
  void do_region(string r1);
  void do_regionor(string r1,string r2);
  void do_regionand(string r1,string r2);
  void do_regionpct(string r1);
  void do_regionpctor(string r1,string r2);
  void do_regionpctand(string r1,string r2);
  double gaussian_random(double sigma);
};

int
main(int argc,char **argv)
{
  if (argc!=2) {
    vbscoregen_help();
    exit(0);
  }
  string arg=argv[1];
  if (arg=="-v") {
    vbscoregen_version();
    exit(0);
  }
  VBscoregen sg(arg);
}



double
VBscoregen::genscore(double pct)
{
  double mean=mean1+(pct*(mean2-mean1));
  double sd=sd1+(pct*(sd2-sd1));
  double ret=mean+gaussian_random(sd);
  if (ret<floor) ret=floor;
  if (ret>ceiling) ret=ceiling;
  return ret;
}

void
VBscoregen::parseconfig(string configfile)
{
  // init stuff here!
  floor=-FLT_MAX;
  ceiling=FLT_MAX;
  seed=VBRandom();
  rng=NULL;

  ifstream infile;
  infile.open(configfile.c_str());
  if (!infile) {
    // FIXME
    exit(111);
  }
  //chdir(xdirname(configfile).c_str());

  char buf[STRINGLEN];
  tokenlist args;
  while (infile.getline(buf,STRINGLEN,'\n')) {
    args.ParseLine(buf);
    if (args.size()==0) continue;
    if (args[0][0]=='#' || args[0][0] == ';')
      continue;
    if (args[0]=="intact" && args.size()==3) {
      mean1=strtod(args[1]);
      sd1=strtod(args[2]);
    }
    else if (args[0]=="damaged" && args.size()==3) {
      mean2=strtod(args[1]);
      sd2=strtod(args[2]);
    }
    else if (args[0]=="bounds" && args.size()==3) {
      floor=strtod(args[1]);
      ceiling=strtod(args[2]);
    }
    else if (args[0]=="seed" && args.size()==2)
      seed=strtol(args[1]);
    else if (args[0]=="mask" && args.size()==3) {
      Cube m;
      if (m.ReadFile(args[2])) {
        // 
        exit (202);
      }
      VBRegion rr=findregion_mask(m,vb_agt,0.0);
      maskmap[args[1]]=rr;
      // cout loaded mask xxxx stats
    }
    else if (args[0]=="voxel" && args.size()==5) {
      VBVoxel vv(strtol(args[2]),strtol(args[3]),strtol(args[4]));
      voxelmap[args[1]]=vv;
      // cout loaded voxel xxxx location
    }
    else if (args[0]=="lesions" && args.size()==2) {
      lesionfile=args[1];
      if (lesions.ReadHeader(args[1])) {
        cout << "[E] vbscoregen: unable to read " << args[1] << endl;
        exit(191);
      }
      scores.resize(lesions.dimt);
    }
    else if (args[0]=="ifvoxel" && args.size()==2) {
      do_ifvoxel(args[1]);
    }
    else if (args[0]=="ifvoxelor" && args.size()==3) {
      do_ifvoxelor(args[1],args[2]);
    }
    else if (args[0]=="ifvoxeland" && args.size()==3) {
      do_ifvoxeland(args[1],args[2]);
    }
    else if (args[0]=="region" && args.size()==2) {
      do_region(args[1]);
    }
    else if (args[0]=="regionor" && args.size()==3) {
      do_regionor(args[1],args[2]);
    }
    else if (args[0]=="regionand" && args.size()==3) {
      do_regionand(args[1],args[2]);
    }
    else if (args[0]=="regionpct" && args.size()==2) {
      do_regionpct(args[1]);
    }
    else if (args[0]=="regionpctor" && args.size()==3) {
      do_regionpctor(args[1],args[2]);
    }
    else if (args[0]=="regionpctand" && args.size()==3) {
      do_regionpctand(args[1],args[2]);
    }
    else if (args[0]=="generate" && args.size()==2) {
      if (scores.size()==0) {
        cout << "[E] vbscoregen: generate line encountered before lesions line\n";
        exit(172);
      }
      if (scores.WriteFile(args[1])) {
        cout << "[E] vbscoregen: error writing score file " << args[1] << endl;
        exit(121);
      }
      else 
        cout << "[I] vbscoregen: wrote score file " << args[1] << endl;
    }
    else {
      infile.close();
      cout << "[E] vbscoregen: unrecognized command " << args[0] << endl;
      exit(128);
    }
  }
  infile.close();
  return;
}


void
VBscoregen::do_ifvoxel(string v1)
{
  if (!voxelmap.count(v1))
    exit(999);
  int x=voxelmap[v1].x;
  int y=voxelmap[v1].y;
  int z=voxelmap[v1].z;
  lesions.ReadTimeSeries(lesionfile,x,y,z);
  for (int i=0; i<lesions.dimt; i++) {
    if (fabs(lesions.timeseries[i])>DBL_MIN)
      scores[i]=genscore(1.0);
    else
      scores[i]=genscore(0.0);
  }
}

void
VBscoregen::do_ifvoxelor(string v1,string v2)
{
  if (!voxelmap.count(v1) || !voxelmap.count(v2))
    exit(999);
  for (int i=0; i<lesions.dimt; i++) {
    if (fabs(lesions.GetValue(voxelmap[v1],i))>DBL_MIN
        ||fabs(lesions.GetValue(voxelmap[v2],i))>DBL_MIN)
      scores[i]=genscore(1.0);
    else
      scores[i]=genscore(0.0);
  }
}

void
VBscoregen::do_ifvoxeland(string v1,string v2)
{
  if (!voxelmap.count(v1) || !voxelmap.count(v2))
    exit(999);
  for (int i=0; i<lesions.dimt; i++) {
    if (fabs(lesions.GetValue(voxelmap[v1],i))>DBL_MIN
        &&fabs(lesions.GetValue(voxelmap[v2],i))>DBL_MIN)
      scores[i]=genscore(1.0);
    else
      scores[i]=genscore(0.0);
  }
}

void
VBscoregen::do_region(string r1)
{
  if (!maskmap.count(r1))
    exit(999);
  VBRegion reg=maskmap[r1];
  for (int i=0; i<lesions.dimt; i++) {
    bool dflg=0;
    for (VI v=reg.begin(); v!=reg.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN) {
        dflg=1;
        break;
      }
    }
    if (dflg)
      scores[i]=genscore(1.0);
    else
      scores[i]=genscore(0.0);
  }
}

void
VBscoregen::do_regionor(string r1,string r2)
{
  if (!maskmap.count(r1) || !maskmap.count(r2))
    exit(999);
  VBRegion reg1=maskmap[r1];
  VBRegion reg2=maskmap[r2];
  for (int i=0; i<lesions.dimt; i++) {
    bool dflg1=0,dflg2=0;
    for (VI v=reg1.begin(); v!=reg1.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN) {
        dflg1=1;
        break;
      }
    }
    if (!dflg1)
      for (VI v=reg2.begin(); v!=reg2.end(); v++) {
        if (fabs(lesions.GetValue(v->second,i))>DBL_MIN) {
          dflg2=1;
          break;
        }
      }
    if (dflg1 || dflg2)
      scores[i]=genscore(1.0);
    else
      scores[i]=genscore(0.0);
  }
}

void
VBscoregen::do_regionand(string r1,string r2)
{
  if (!maskmap.count(r1) || !maskmap.count(r2))
    exit(999);
  VBRegion reg1=maskmap[r1];
  VBRegion reg2=maskmap[r2];
  for (int i=0; i<lesions.dimt; i++) {
    bool dflg1=0,dflg2=0;
    for (VI v=reg1.begin(); v!=reg1.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN) {
        dflg1=1;
        break;
      }
    }
    if (dflg1)
      for (VI v=reg2.begin(); v!=reg2.end(); v++) {
        if (fabs(lesions.GetValue(v->second,i))>DBL_MIN) {
          dflg2=1;
          break;
        }
      }
    if (dflg1 && dflg2)
      scores[i]=genscore(1.0);
    else
      scores[i]=genscore(0.0);
  }
}

void
VBscoregen::do_regionpct(string r1)
{
  if (!maskmap.count(r1))
    exit(999);
  VBRegion reg1=maskmap[r1];
  for (int i=0; i<lesions.dimt; i++) {
    uint32 cnt=0;
    for (VI v=reg1.begin(); v!=reg1.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN)
        cnt++;
    }
    scores[i]=genscore((double)cnt/(double)reg1.size());
  }
}

void
VBscoregen::do_regionpctor(string r1,string r2)
{
  if (!maskmap.count(r1) || !maskmap.count(r2))
    exit(999);
  VBRegion reg1=maskmap[r1];
  VBRegion reg2=maskmap[r2];
  for (int i=0; i<lesions.dimt; i++) {
    double pct1,pct2;
    uint32 cnt=0;
    for (VI v=reg1.begin(); v!=reg1.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN)
        cnt++;
    }
    pct1=(double)cnt/(double)reg1.size();
    for (VI v=reg2.begin(); v!=reg2.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN)
        cnt++;
    }
    pct2=(double)cnt/(double)reg2.size();
    scores[i]=genscore((pct2>pct1 ? pct2 : pct1));
  }
}

void
VBscoregen::do_regionpctand(string r1,string r2)
{
  if (!maskmap.count(r1) || !maskmap.count(r2))
    exit(999);
  VBRegion reg1=maskmap[r1];
  VBRegion reg2=maskmap[r2];
  for (int i=0; i<lesions.dimt; i++) {
    double pct1,pct2;
    uint32 cnt=0;
    for (VI v=reg1.begin(); v!=reg1.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN)
        cnt++;
    }
    pct1=(double)cnt/(double)r1.size();
    for (VI v=reg2.begin(); v!=reg2.end(); v++) {
      if (fabs(lesions.GetValue(v->second,i))>DBL_MIN)
        cnt++;
    }
    pct2=(double)cnt/(double)reg2.size();
    scores[i]=genscore((pct2>pct1 ? pct1 : pct2));
  }
}



double
VBscoregen::gaussian_random(double sigma)
{
  if (!rng) {
    rng=gsl_rng_alloc(gsl_rng_mt19937);
    assert(rng);
    gsl_rng_set(rng,seed);
  }
  return gsl_ran_gaussian(rng,sigma);
}




void
vbscoregen_help()
{
  cout << boost::format(myhelp) % vbversion;
}

void
vbscoregen_version()
{
  printf("VoxBo vbscoregen (v%s)\n",vbversion.c_str());
}
