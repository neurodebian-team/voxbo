
// vbcmp.cpp
// compare 3d and 4d files, data only
// Copyright (c) 1998-2011 by The VoxBo Development Team

// VoxBo is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// VoxBo is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with VoxBo.  If not, see <http://www.gnu.org/licenses/>.
// 
// For general information on VoxBo, including the latest complete
// source code and binary distributions, manual, and associated files,
// see the VoxBo home page at: http://www.voxbo.org/
//
// original version written by Dan Kimberg

#include <stdio.h>
#include <string.h>
#include <math.h>
#include "vbutil.h"
#include "vbio.h"
#include "vbcmp.hlp.h"

using namespace std;

void vbcmp_help();
void vbcmp_version();
void cmp1d(VB_Vector &v1,VB_Vector &v2);
void cmp2d(VBMatrix &mat1,VBMatrix &mat2);
void cmp3d(Cube &c1,Cube &c2);
void cmp4d(Tes &t1,Tes &t2);

int
main(int argc,char *argv[])
{
  tokenlist args;
  string infile,outfile;

  args.Transfer(argc-1,argv+1);

  if (args.size() == 0) {
    vbcmp_help();
    exit(0);
  }

  if (args.size() != 2) {
    vbcmp_help();
    exit(5);
  }

  int err1,err2;
  Cube cb1,cb2;
  err1=cb1.ReadFile(args[0]);
  err2=cb2.ReadFile(args[1]);
  if (!err1 && !err2) {
    cmp3d(cb1,cb2);
    exit(0);
  }
  Tes ts1,ts2;
  err1=ts1.ReadFile(args[0]);
  err2=ts2.ReadFile(args[1]); 
  if (!err1 && !err2) {
    cmp4d(ts1,ts2);
    exit(0);
  }
  // try vectors before matrices, because all vectors are matrices
  VB_Vector v1,v2;
  err1=v1.ReadFile(args[0]);
  err2=v2.ReadFile(args[1]);
  if (!err1 && !err2) {
    cmp1d(v1,v2);
    exit(0);
  }
  VBMatrix m1,m2;
  err1=m1.ReadFile(args[0]);
  err2=m2.ReadFile(args[1]);
  if (!err1 && !err2) {
    cmp2d(m1,m2);
    exit(0);
  }
  printf("[E] vbcmp: couldn't read both volumes as same type\n");
  exit(100);
}

void
cmp1d(VB_Vector &v1,VB_Vector &v2)
{
  if (v1.size()!=v2.size()) {
    printf("[E] vbcmp: matrices have different row count\n");
    return;
  }

  int diffs_all=0;
  double totals_all=0.0,max_all=0.0;
  double diff;
  for (uint32 i=0; i<v1.size(); i++) {
    diff=fabs(v1(i)-v2(i));
    if (diff==0.0) continue;
    diffs_all++;
    totals_all+=diff;
    if (diff>max_all) max_all=diff;
  }
  if (diffs_all)
    totals_all/=(double)diffs_all;
  if (diffs_all==0)
    printf("[I] vbcmp: vectors are identical\n");
  else {
    printf("[I] vbcmp: %d total elements\n",(int)v1.size());
    printf("[I] vbcmp:    total: %d different elements, mean abs diff %g, max diff %g\n",diffs_all,totals_all,max_all);
    printf("[I] vbcmp:    correlation between vectors (Pearson's r) is: %g\n",correlation(v1,v2));
  }
}

// the below should be kept in sync with similar code in vbmm2.cpp

void
cmp2d(VBMatrix &mat1,VBMatrix &mat2)
{
  if (mat1.m!=mat2.m) {
    printf("[E] vbcmp: matrices have different row count\n");
    return;
  }
  if (mat1.n!=mat2.n) {
    printf("[E] vbcmp: matrices have different column count\n");
    return;
  }
  int diffs_all=0,diffs_diag=0,diffs_off=0;
  double totals_all=0.0,totals_diag=0.0,totals_off=0.0;
  double max_all=0.0,max_diag=0.0,max_off=0.0;
  double diff;
  
  for (uint32 i=0; i<mat1.m; i++) {
    for (uint32 j=0; j<mat1.n; j++) {
      diff=fabs(mat1(i,j)-mat2(i,j));
      if (diff==0.0) continue;
      diffs_all++;
      totals_all+=diff;
      if (diff>max_all) max_all=diff;
      if (i==j) {
        diffs_diag++;
        totals_diag+=diff;
        if (diff>max_diag) max_diag=diff;
      }
      else {
        diffs_off++;
        totals_off+=diff;
        if (diff>max_off) max_off=diff;
      }
    }
  }
  if (diffs_all)
    totals_all/=(double)diffs_all;
  if (diffs_diag)
    totals_diag/=(double)diffs_diag;
  if (diffs_off)
    totals_off/=(double)diffs_off;
  if (diffs_all==0)
    printf("[I] vbcmp: matrices are identical\n");
  else {
    printf("[I] vbcmp: out of %d total cells:\n",mat1.m*mat1.n);
    printf("[I] vbcmp:    total: %d different cells, mean abs diff %g, max diff %g\n",diffs_all,totals_all,max_all);
    if (mat1.m==mat1.n) {
      printf("[I] vbcmp: diagonal: %d different cells, mean abs diff %g, max diff %g\n",diffs_diag,totals_diag,max_diag);
      printf("[I] vbcmp: off-diag: %d different cells, mean abs diff %g, max diff %g\n",diffs_off,totals_off,max_off);
    }
  }
}

void
cmp3d(Cube &c1,Cube &c2)
{
  if (c1.dimx != c2.dimx || c1.dimy != c2.dimy || c1.dimz != c2.dimz) {
    printf("[I] vbcmp: dimensions don't match, can't compare.\n");
    return;
  }
  double totaldiff,maxdiff=0.0,v1,v2,diff;
  int32 diffcount,voxelcount;
  int i,j,k;

  diffcount=0;
  totaldiff=0.0;
  int maxx=0,maxy=0,maxz=0;
  voxelcount=c1.dimx * c1.dimy * c1.dimz;
  for (i=0; i<c1.dimx; i++) {
    for (j=0; j<c1.dimy; j++) {
      for (k=0; k<c1.dimz; k++) {
        v1=c1.GetValue(i,j,k);
        v2=c2.GetValue(i,j,k);
        if (v1 != v2) {
          if (isnan(v1) && isnan(v2)) continue;
          if (isinf(v1) && isinf(v2)) continue;
          diffcount++;
          diff=fabs(v1-v2);
          totaldiff += diff;
          if (diff>maxdiff) {
            maxdiff=diff;
            maxx=i;
            maxy=j;
            maxz=k;
          }
        }
      }
    }
  }
  if (!diffcount) {
    printf("[I] vbcmp: the data are identical\n");
  }
  else {
    cout << format("[I] vbcmp: different voxels: %ld of %ld (%.0f%%)\n")
      % diffcount % voxelcount % ((diffcount*100.0)/voxelcount);
    cout << format("[I] vbcmp: mean difference: %.8f\n") % (totaldiff/diffcount);
    cout << format("[I] vbcmp: max difference: %.8f (%d,%d,%d)\n")
      % maxdiff % maxx % maxy % maxz;
  }
  exit(0);
}

void
cmp4d(Tes &t1,Tes &t2)
{
  if (t1.dimx != t2.dimx || t1.dimy != t2.dimy || t1.dimz != t2.dimz
      || t1.dimt != t2.dimt) {
    cout << format("[I] vbcmp: dimensions don't match, can't compare.\n");
    return;
  }
  double totaldiff,maxdiff=0.0,v1,v2,diff;
  long diffcount,voxelcount;
  int i,j,k,m,voldiff;
  int differingvolumes=0;

  diffcount=0;
  totaldiff=0.0;
  voxelcount=t1.dimx * t1.dimy * t1.dimz * t1.dimt;
  for (m=0; m<t1.dimt; m++) {
    voldiff=0;
    for (i=0; i<t1.dimx; i++) {
      for (j=0; j<t1.dimy; j++) {
        for (k=0; k<t1.dimz; k++) {
          v1=t1.GetValue(i,j,k,m);
          v2=t2.GetValue(i,j,k,m);
          if (v1 != v2) {
            voldiff=1;
            diffcount++;
            diff=fabs(v1-v2);
            totaldiff += diff;
            if (diff>maxdiff) maxdiff=diff;
          }
        }
      }
    }
    if (voldiff)
      differingvolumes++;
  }
  if (!diffcount) {
    printf("[I] vbcmp: the data are identical.\n");
  }
  else {
    printf("[I] vbcmp: different voxels: %ld of %ld (%.0f%%)\n",diffcount,voxelcount,
	   (double)(diffcount*100.0)/voxelcount);
    printf("[I] vbcmp: mean difference: %.8f\n",totaldiff/diffcount);
    printf("[I] vbcmp: max difference: %.8f\n",maxdiff);
    printf("[I] vbcmp: %d of %d volumes differ\n",differingvolumes,t1.dimt);
  }
  exit(0);
}

void
vbcmp_help()
{
  cout << boost::format(myhelp) % vbversion;
}

void
vbcmp_version()
{
  printf("\nVoxBo vbcmp (v%s)\n",vbversion.c_str());
}
